#!/bin/sh
set -e

file_name=openmw-books-enhanced.zip

cat > version.txt <<EOF
Mod version: $(git describe --tags)
EOF

zip --must-match --recurse-paths ${file_name} \
    CHANGELOG.md \
    LICENSE \
    README.md \
    l10n \
    openmw_books_enhanced.omwscripts \
    scripts \
    textures \
    docs \
    version.txt

sha256sum ${file_name} > ${file_name}.sha256sum.txt
sha512sum ${file_name} > ${file_name}.sha512sum.txt
