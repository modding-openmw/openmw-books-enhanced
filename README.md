# trav's OpenMW Books Enhanced

(**Requires OpenMW version 0.49+** -- available as *development build* at https://openmw.org/downloads/)

This mod replaces the book/scroll in-game UI with a custom version mimicking the vanilla game look, but with additional expansions.

## Features

### 1. **Configurable window & font size**
With a smaller font, now more lines can fit into a book page. You can set the font size and the book/scroll width & height to any value you want.

![You can set books & their font to any size](docs/tobe_01.png)

### 2. ***(optional)* Daedric text translator**
A new option allows to change all Daedric text (for example: in spell scrolls) to use a Battlespire font with latin letters, making the text human-readable. By default this option is disabled, but you can change it in the script settings at any time.

![Daedric/Battlespire font can be switched in options](docs/tobe_02.png)

### 3. **Colored book covers**

An optional setting colors the book spines in the book UI to fit the colors on the game models. Vanilla Morrowind, Tamriel Data & OAAB Data books supported.

![Vanilla & TR books have colored covers](docs/tobe_04.png)

### 4. **Support for custom book layouts**
A small external API allows to create a custom book layout for any condition the book item fulfils. For example: you can make a custom book skin for *The 36 Lessons of Vivec*, or for any book item using a specific model, or anything else the OpenMW `types.Book` API allows for. I may make more custom layouts in the future, but I also welcome you to make your own expansions if you like. See details [here](https://gitlab.com/modding-openmw/openmw-books-enhanced/-/blob/master/docs/Custom-book-layouts.md)

For example, this mod implements a unique one-page book UI for the *The Ebonheart Bellman* newspaper and a UI for the palm-leaf manuscripts in the Tamriel Rebuilt mod.

![Custom book UI layout example1](docs/tobe_03.png)

![Custom book UI layout example2](docs/tobe_05.png)

### 5. **High-res replacements of the vanilla book/scroll UI textures**

### 6. **Keyboard & gamepad support**
Like in vanilla OpenMW, you can navigate through the opened book/scroll with a keyboard (WSAD/Arrows+E/Enter) and you can navigate with a gamepad (D-Pad+A for buttons and shoulder buttons for page scrolling). Warning: it's not fully working correctly due to OpenMW limitations - see known issues below.

### 7. **Equip a spell when equipping the book/scroll enchanted with it**
If a book/scroll is enchanted (magic scrolls count), the spell bound to it is equipped when reading. You can disable that option in the settings.

### 8. **Display an indicator when looking at books/scrolls which have been already read**
An optional eye indicator (position, size and color are configurable) can be displayed when you point a crosshair at a book/scroll which you have already read. *Note: items opened before the installation of this mod do **not** count.*

![Book read indicator icon - an eye next to the crosshair](docs/tobe_06.png)

## Installation

1. Download the mod from [this URL](https://modding-openmw.gitlab.io/openmw-books-enhanced/)
1. Extract the zip to a location of your choosing, examples below:

        # Windows
        C:\games\OpenMWMods\openmw-books-enhanced

        # Linux
        /home/username/games/OpenMWMods/openmw-books-enhanced

        # macOS
        /Users/username/games/OpenMWMods/openmw-books-enhanced

1. Add the appropriate data path to your `openmw.cfg` file (e.g. `data="C:\games\OpenMWMods\openmw-books-enhanced"`)
1. Add `openmw_books_enhanced.omwscripts` to your load order in `openmw.cfg` or enable them via OpenMW-Launcher

## Known issues and things worth mentioning

### Blurry text

Current OpenMW implementation uses the same prepared font size for every UI element, including custom Lua UI layouts like the additions made by this mod. The crispy font used in vanilla game books is hardcoded. However, you can manually change the Lua UI elements to use it by doing this:

- Go to your OpenMW installation folder
- Open the `resources\vfs\mygui\openmw_lua.xml` file
- You should see these lines:

```xml
<Resource type="ResourceSkin" name="LuaText" size="16 16">
  <Property key="FontName" value="Default"/>
[...]
<Resource type="ResourceSkin" name="LuaTextEdit" size="16 16">
  <Property key="FontName" value="Default"/>
```

- Change both these `Default`s to `Journalbook DefaultFont`, so that you see:

```xml
<Resource type="ResourceSkin" name="LuaText" size="16 16">
  <Property key="FontName" value="Journalbook DefaultFont"/>
[...]
<Resource type="ResourceSkin" name="LuaTextEdit" size="16 16">
  <Property key="FontName" value="Journalbook DefaultFont"/>
```

Expected result compared to default:

![Font comparison](docs/tobe_fontHack.png)

*WARNING:* This shall change the font in all Lua-added OpenMW UI elements, not just this mod! Do it at your own "risk". You can always revert the change by setting the values back to `Default`s

### Book/scroll window texture

I made the high-res textures replacing the book & scroll UI. Feel free to remove them (they are in the `textures` folder) or replace them with any texture you prefer by loading a mod after this one. Here are my few recommendations:

- The three "Vanilla Style" mods combined:
  - *Vanilla Style Scroll* - https://www.nexusmods.com/morrowind/mods/54476
  - *Vanilla Style Book* - https://www.nexusmods.com/morrowind/mods/54004
  - *Vanilla Style Bookmark* - https://www.nexusmods.com/morrowind/mods/54477
- *Intelligent Textures* offers a nice book UI replacement - https://www.nexusmods.com/morrowind/mods/47469

However, if you do that, remember to disable the "Use autocolored book covers" option in the mod settings!

### No support for quick keys

If you bind a book to a Quick Key and try to open it this way, you should see a warning window saying to close the book and open it manually in the inventory. This is because the current OpenMW Lua API does not allow to detect when an item is activated via Quick Keys. This may change in the future.

### Reading during battle

Vanilla OpenMW does not allow you to read books from your inventory during battle. As of now, this mod allows it, even though you will see a warning that you can't. This is because the current OpenMW Lua API does not allow to (easily) detect when the player is in combat. This may change in the future.

### Up/Down scrollbar in the scroll UI window doesn't work

It's actually just for show - only the Up/Down buttons work. This is because the current OpenMW Lua API does not have a scrolling UI functionality. This may change in the future.

You can use the mouse wheel or Up/Down (W/S) keyboard keys though.

### (Keyboard+Controller issue) Issue with window focus

Even though you can use a keyboard to navigate through a book/scroll, ***you still need to click at least once on the UI*** to gain focus, otherwise the game will still think you want to close the window. This is either because the current OpenMW Lua API does not allow to force the focus on a UI element, or doesn't "remove" an old game window properly, or I can't do programming. This may change in the future.

### (Keyboard+Controller issue) Issue with an 'Activate' key

Current OpenMW Lua API does not allow to detect when an "Activate" action is pressed by the player, so the mod considers only the `E` or `Enter` to be the keyboard keys used to confirm the selected choice or the `(A)` button on the gamepad. If you bind "Activate" to some other key, you still can use unly the `E`/`Enter`/`(A)`. This may change in the future.

### Stolen item is not confiscated

When you steal a book/scroll, the basic bounty mechanic should work and the guards should chase you if they see it, but they won't confiscate your stolen item, since it's not considered 'stolen' anymore. This may change in the future if OpenMW Lua API offers a proper crime framework.

### The read indicator is visible even if you disable the HUD by F11

It's a known OpenMW issue that custom UI elements can't be hidden by F11 key:

- https://gitlab.com/OpenMW/openmw/-/issues/7578
- https://gitlab.com/OpenMW/openmw/-/issues/8016

This may change if these issues are resolved, but at the moment the only way is to move the viewpoint away to hide the indicator (or disable it in settings).

### . (period) symbol can't be typed into the number settings for this (or any other) script

It's a known issue https://gitlab.com/OpenMW/openmw/-/issues/8116 that OpenMW is using the `.` and `/` keyboard buttons for something else, so you can't use them as typed-in symbols in some input boxes. This means that in the mod settings you can't manually write the fraction separator to have for example `1.23`, unless you are using a keyboard setting (like English keyboards) which types the period when preessing the `Del` key on the numpad. Until that issue is fixed in OpenMW, the only other way is to copy the period symbol from somewhere else (even from outside OpenMW) and pasting it manually in the setting.

### Text does not fit into the readable space (it's cut off) when using custom fonts

This mod was made specifically with the MysticCards/Pelagiad font in mind - this is the font natively used by OpenMW. I had to manually precalculate the widths of all symbols from that fonts to check where the lines end. If you change the OpenMW font, you are entering the unsupported territory: depending on the font, everything may still look fine, but you may as well experience issues. As of now I offer no 'official' support for custom fonts, but if you want to try your own fixes, then find and change the numbers in the character_sizing.lua file.

## Credits

Author: **trav** (https://gitlab.com/trav55)

Thanks to:

- **@zackhasacat** for the Lua stealing-detection hacky-but-working functionality idea
- **[pwn](https://next.nexusmods.com/profile/ownlyme/mods)** for the idea to use [BMFont](https://www.angelcode.com/products/bmfont/) to generate a font texture atlas & for the Lua function used to organize the extracted data to define Daedric fonts used in this mod & for the raycasting function used in [QuickLoot](https://www.nexusmods.com/morrowind/mods/54926)
- **Honken** at the OpenMW #skandinaviskt Discord community for the assistance in the Swedish translation
- **@hristoast** & the modding-openmw community for allowing me to host at the Modding-OpenMW gitlab group
- **Oreman** for the Chinese translation

## Report A Problem

If you've found an issue with this mod, or if you simply have a question, please use one of the following ways to reach out:

* [Open an issue on GitLab](https://gitlab.com/modding-openmw/openmw-books-enhanced/-/issues)
* Email `travv55055@gmail.com`
* Contact the author on Discord: `@trav_5`