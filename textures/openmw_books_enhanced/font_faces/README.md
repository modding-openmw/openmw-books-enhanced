All fonts in this directory are converted from TrueType to bitmaps by BMFont - https://www.angelcode.com/products/bmfont/. Idea given by @pwn. Thanks!

# DemonicLetters.fnt & DemonicLetters_0.dds

These are vanilla daedric letters, i.e. the *Demonic Letters* font from OpenMW.

https://gitlab.com/OpenMW/openmw/-/blob/master/files/data/fonts/DemonicLettersFontLicense.txt

# PentaGramMalefissent.fnt & PentaGramMalefissent_0.dds

According to UESP, this was the font used for Battlespire box art - https://en.uesp.net/wiki/General:Fonts. I'm using it as a "human-readable" version of the daedric texts. PentaGram's Malefissent by Theyann PentaGram: https://www.1001freefonts.com/pentagram-s-malefissent.font & http://luc.devroye.org/fonts-68102.html