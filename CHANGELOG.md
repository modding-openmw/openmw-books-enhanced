## trav's OpenMW Books Enhanced Changelog

#### Version 3.0

Initial version of the mod published on a gitlab page.

#### Version 4.0

- Added a new optional feature: Spells bound to enchanted items (books/scrolls) are equipped when reading them.
- Added more book colors: assets from High Rock 427 and OAAB Data

#### Version 5.0

- Added support (at least partial) for UTF-8 text contents, like the [Unofficial Chinese localisation](https://www.nexusmods.com/morrowind/mods/53885)
  - Added a Chinese localisation for this mod as well. Thanks, Oreman, for providing it!
- Added an optional indicator which is displayed if you point a crosshair at a book/scroll you have read before.
- Improved UI textures with a better alpha channel
- Bugfixes:
  - Taking a disabled item shouldn't put it into inventory
  - Fixed interaction with [Imperial Factions](https://modding-openmw.com/mods/imperial-factions/) - book UI can be overriden by a game script

#### Version 6.0

- First version also uploaded to [Nexus](https://www.nexusmods.com/morrowind/mods/55126)
- Fixed the magenta issue, where the mod wasn't able to load the bookart from BSA files, because they are dds, when the mod expected tga.

#### Version 6.1

- Fixed an issue when trying to steal an item with no value would not allow to take the item.
- Set the default screen width/height multiplier to 2.01, so that a period (dot) would be visible in the settings for people who can't type it due to an OpenMW bug.
- Wrote explanations in the known issues about not being able to type in period symbols and about custom fonts.
- Added automatic size support for 1980x1080 resolution. I can't test bigger screens though.
- Added custom UIs for more newspaper books: The Common Tongue from Tribunal and the Canyon Echo from the upcoming Tamriel Rebuilt expansion.
- Added compatibility with [QuickLoot](https://www.nexusmods.com/morrowind/mods/54950) which also gathers the info about books you have read before. Now this mod should also check if the book is known by looking through data gathered by QuickLoot.
