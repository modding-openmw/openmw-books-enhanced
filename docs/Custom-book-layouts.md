
([back to README](../README.md))

## Custom book/scroll UI template making

This mod provides the API to make your own book/scroll UIs. Here is some documentation on it.

Each custom template is a lua script placed in the `scripts/data_for_openmw_books_enhanced` folder (not necessarily in this mod - other mods can have this too - the script vfs should find it, as long as the path matches).

The last template applicable to the opened book/scroll shall be applied.

In the template you define the positions of each widget, the path to the texture, the condition which the book item must fulfil so that template is used and some other optional data - see below:

## Example

```lua
local DocumentData = {
    name = NAME,
    shouldApplyTo = function(gameObject)
        return CONDITION
    end,
    texture = {
        path = TEXTURE,
        width = A,
        height = B,
        colorable = SEE_COLORING,
    },
    pagesTextArrangement = {
        page1 = {
            textArea = {
                width = C,
                height = D,
                posTopLeftCornerX = E,
                posTopLeftCornerY = F,
                isScrollableVertically = SCROLLABLE,
            },
            pageNumber = {
                posCenterX = G,
                posCenterY = H,
            }
        },
        page2 = {
            textArea = {
                posTopLeftCornerX = I,
                posTopLeftCornerY = J,
            },
            pageNumber = {
                posCenterX = K,
                posCenterY = L,
            }
        },
    },
    takeButton = {
        posCenterX = M,
        posCenterY = N,
    },
    prevButton = {
        posCenterX = O,
        posCenterY = P,
    },
    nextButton = {
        posCenterX = Q,
        posCenterY = R,
    },
    closeButton = {
        posCenterX = S,
        posCenterY = T,
    },
    modifyTextBeforeApplying = function(text)
        return TEXT_MODIFIED
    end,
    additionalWidgetsInDocumentUi = {
        WIDGETS
    },
    getUiColoring = function(gameObject)
        return SEE_COLORING
    end,
}
return DocumentData
```

### Explanations for letter coordinates

![All letter values](tobe_customUIGuide.png)

### Explanations for names parameters and functions:

- **`NAME`** - a unique string name for your template
- **`shouldApplyTo` & `CONDITION`** - `shouldApplyTo` function is given the [GameObject](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/openmw_core.html##(GameObject)) of the used book/scroll item as an argument. The function should return `true` if the template can be applied to the item, or false if it can't.
- **`TEXTURE`** - path to the main texture for the UI. Shall be scaled by values of `A` & `B`.
- **`SCROLLABLE`** - Set to `true`/`false`. If `true`, then the UI shall be scrollable up/down instead of being split to pages. With this setting enabled you probably should remove the whole `page2` part.
- **`modifyTextBeforeApplying` & `TEXT_MODIFIED`** - an optional function - you can remove it if you don't need it. It takes the book/scroll text as an argument and you can change it in any way you want before it's rendered - and return the modified result as `TEXT_MODIFIED`
- **`additionalWidgetsInDocumentUi`** - an optional table - you can remove it if you don't need it. You can add your custom [widgets](https://openmw.readthedocs.io/en/latest/reference/lua-scripting/user_interface.html#widget-types) to it - they shall be placed inside the main book/scroll UI.
- **`getUiColoring` & `texture.colorable`** - You can set a `colorable` boolean to `true` in the main UI texture. This shall enable the usage of custom coloring - you have to define a `getUiColoring` function, which returns a color based on the provided book/scroll game object. It can also return `nil`, which shall make the mod revert back to the vanilla book color. Is you use this setting, you have to provide three texture files instead of one:
  - Main **`TEXTURE`**, which shall be used in case the coloring does not apply;
  - A monochrome texture on which the colors shall be applied - it should be in the same directory as the main **`TEXTURE`** and the name needs to have the suffix `_colorable`;
  - A texture which goes on top of the monochrome texture and is not colored - it should be in the same directory as the main **`TEXTURE`** and the name needs to have the suffix `_nonColorable`.


When in doubt, see the example templates in this mod, available [here](../scripts/data_for_openmw_books_enhanced). Basically three custom templates are (intentionally) supported:
- a two-page book-like template (`0100_vanilla_book.lua`)
- a one-page book-like template - same as above but with only one page visible (`0300_tr_news_bellman.lua`)
- a scroll-like template with no page turning but with page scrolling up/down (`0200_vanilla_scroll.lua`)